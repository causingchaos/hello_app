class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: "Ovarian Cancer Student Action Club \n\n Website under development \n Aug 9th, 2017!"
  end

  def goodbye
    render html: "goodbye, world!"
  end

end
